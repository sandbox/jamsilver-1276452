<?php

/**
 * @file
 * Builds placeholder replacement tokens for views.
 */

/**
 * Implements hook_token_info().
 */
function token_views_token_info() {
  
  $info = array();
  
  $info['types']['view'] = array(
    'name' => t('View'),
    'description' => t('Tokens related to views.'),
    'needs-data' => 'view',
  );
  $info['tokens']['view']['machine-name'] = array(
    'name' => t('Machine name'),
    'description' => t('The machine name of the view.'),
  );
  $info['tokens']['view']['name'] = array(
    'name' => t('Name'),
    'description' => t('The human-readable name of the view.'),
  );
  $info['tokens']['view']['row-count'] = array(
    'name' => t('Row count'),
    'description' => t('The result count of the currently active display.'),
  );
  //$info['tokens']['view']['arg'] = array(
  //  'name' => t('View argument'),
  //  'description' => t("The specific argument of the view indexed from 0."),
  //  'dynamic' => TRUE,
  //);
  //$info['tokens']['view']['filter'] = array(
  //  'name' => t('View filter value'),
  //  'description' => t("The specific filter value of the view indexed from 0."),
  //  'dynamic' => TRUE,
  //);
  //$info['tokens']['current-page']['view'] = array(
  //  'name' => t('Views page view'),
  //  'description' => t('The view providing the current page, if there is one.'),
  //  'type' => 'view',
  //);
  //$info['tokens']['view']['current-display'] = array(
  //  'name' => t('Current display'),
  //  'description' => t('The currently active display of the view.'),
  //  'type' => 'view-display',
  //);
  //$info['tokens']['view']['display'] = array(
  //  'name' => t('Link ID'),
  //  'description' => t('The unique ID of the menu link.'),
  //  'type' => 'view-display',
  //  'dynamic' => TRUE,
  //);
  //
  //$info['types']['view-display'] = array(
  //  'name' => t('Views view display'),
  //  'description' => t('Tokens related to a certain display of a view.'),
  //  'needs-data' => 'view',
  //);
  
  return $info;
}

/**
 * Implements hook_token_values().
 */
function token_views_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  if ($type == 'view' && !empty($data['view'])) {
    $view = $data['view'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'machine-name':
          $replacements[$original] = $sanitize ? filter_xss($view->name) : $view->name;
          break;
        case 'name':
          $replacements[$original] = $sanitize ? filter_xss($view->human_name) : $view->human_name;
          break;
        case 'row-count':
          $count = is_numeric($view->total_rows) ? $view->total_rows : (!empty($view->query->pager->total_items) ? $view->query->pager->total_items : NULL);
          $replacements[$original] = isset($count) ? $count : '';
          break;
      }
    }
  }

  return $replacements;
}
